CREATE TABLE person(
id INT NOT NULL PRIMARY KEY,
first_name VARCHAR(255),
last_name VARCHAR(255),
email VARCHAR(255)
);

CREATE TABLE merchant(
                       id INT NOT NULL PRIMARY KEY,
                       merchant_name VARCHAR(255),
                       person_id INT

);


INSERT INTO person VALUES(1, 'Varun', 'Kumar', 'varun@gmail.com');
INSERT INTO person VALUES(2, 'Vishal', 'Kumar', 'vishal@gmail.com');
INSERT INTO person VALUES(3, 'Vijay', 'Kumar', 'vijay@gmail.com');

INSERT INTO merchant VALUES(4, 'apple', 1);
INSERT INTO merchant VALUES(5, 'apple', 2);
INSERT INTO merchant VALUES(6, 'natwest', 3);
INSERT INTO merchant VALUES(7, 'lyod', 3);
