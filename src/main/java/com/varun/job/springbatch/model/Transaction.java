package com.varun.job.springbatch.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    private String username;
    private String userId;
    private int age;
    private String postCode;
    private LocalDateTime transactionDate;
    private double amount;
}
