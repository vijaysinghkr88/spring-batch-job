package com.varun.job.springbatch.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonMerchant {
    private int id;
    private String first_name;
    private String last_name;
    private String email;
    private String merchant_name;
    private String person_id;
}
