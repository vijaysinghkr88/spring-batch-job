package com.varun.job.springbatch.config;

import com.varun.job.springbatch.model.PersonMerchant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RowMapperImpl implements RowMapper<PersonMerchant> {
    private static final String ID = "id";
    private static final String  MERCHANT_NAME = "merchant_name";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    //private static final String PERSON_ID = "person_id";
    @Override
    public PersonMerchant mapRow(ResultSet resultSet, int i) throws SQLException {
        System.out.println("next");
        PersonMerchant personMerchant = new PersonMerchant();
        personMerchant.setId(Integer.parseInt(resultSet.getString(ID)));
        personMerchant.setMerchant_name(resultSet.getString(FIRST_NAME) + " " + resultSet.getString(LAST_NAME));
        //personMerchant.setLast_name(resultSet.getString("last_name"));
        personMerchant.setPerson_id(resultSet.getString(ID));
        //personMerchant.setMerchant_name("varun");
        return personMerchant;
    }
}
