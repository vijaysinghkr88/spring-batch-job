package com.varun.job.springbatch.config;

import com.varun.job.springbatch.model.Person;
import com.varun.job.springbatch.model.PersonMerchant;
import com.varun.job.springbatch.processor.PersonItemProcessor;
import com.varun.job.springbatch.processor.PersonMerchantItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.database.builder.JdbcPagingItemReaderBuilder;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableBatchProcessing
@EnableScheduling
@Component
public class SpringBatchConfigSQL {
    @Autowired
    private JobBuilderFactory jobs;
    @Autowired
    private StepBuilderFactory steps;

    //@Autowired
    //private SpringConfig springConfig;
    //@Autowired
    //private PersonItemProcessor personItemProcessor;
    /*@Autowired
    @Lazy
    private JobCompletionNotificationListener jobCompletionNotificationListener;*/

    /*@Bean
    @StepScope
    public JdbcCursorItemReader<PersonMerchant> personMerchantItemReader() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        //dataSource.
        dataSource.setUrl("jdbc:mysql://localhost:3306/mybatchjob?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("testing");
        return new JdbcCursorItemReaderBuilder<PersonMerchant>()
                .name("xx")
                .dataSource(dataSource)
                .sql("SELECT id From person")
                .beanRowMapper(RowMapperImpl.class)
                .maxRows(2)
                .fetchSize(1)
                .build();
    }*/
    @Bean
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        //dataSource.
        dataSource.setUrl("jdbc:mysql://localhost:3306/mybatchjob?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("testing");
        return dataSource;
    }

    @Bean
    public JdbcCursorItemReader<PersonMerchant> itemReader() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        //dataSource.
        dataSource.setUrl("jdbc:mysql://localhost:3306/mybatchjob?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("testing");
        return new JdbcCursorItemReaderBuilder<PersonMerchant>()
                .dataSource(dataSource)
                .name("creditReader")
                .sql("SELECT * From person")
                .fetchSize(1)
                .maxRows(3)
                .rowMapper(new RowMapperImpl())
                .build();

    }

    @Bean
    public JdbcPagingItemReader<PersonMerchant> itemReader1() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        //dataSource.
        dataSource.setUrl("jdbc:mysql://localhost:3306/mybatchjob?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("testing");

        Map<String, Order> sortKeys= new HashMap<>();
        sortKeys.put("sb_sort_column", Order.ASCENDING);
        MySqlPagingQueryProvider sqlPagingQueryProviderFactoryBean = new MySqlPagingQueryProvider();
        sqlPagingQueryProviderFactoryBean.setSelectClause("select *");
        sqlPagingQueryProviderFactoryBean.setFromClause("from person");
        sqlPagingQueryProviderFactoryBean.setSortKeys(sortKeys);
        return new JdbcPagingItemReaderBuilder<PersonMerchant>()
                .name("paging")
                .dataSource(dataSource)
                .queryProvider(sqlPagingQueryProviderFactoryBean)
                .pageSize(1)
                .fetchSize(1)
                .rowMapper(new RowMapperImpl())
                .build();
        /*return new JdbcCursorItemReaderBuilder<PersonMerchant>()
                .dataSource(dataSource)
                .name("creditReader")
                .sql("SELECT * From person")
                .fetchSize(1)
                .maxRows(3)
                .rowMapper(new RowMapperImpl())
                .build();*/

    }

    @Bean
    @StepScope
    public JdbcBatchItemWriter<PersonMerchant> writer(DataSource dataSource) {
        System.out.println("writer");
        return new JdbcBatchItemWriterBuilder<PersonMerchant>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO merchant (id, merchant_name, person_id) VALUES (:id, :merchant_name, :person_id)")
                .dataSource(dataSource)
                .build();
    }


    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate =  new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

    @Bean
    @StepScope
    public ItemProcessor<PersonMerchant, PersonMerchant> itemProcessor() {
        return new PersonMerchantItemProcessor();
    }

   /* @Bean
    public JobCompletionNotificationListener jobCompletionNotificationListener() {
        return new JobCompletionNotificationListener(jdbcTemplate());
    }*/



    @Bean(name = "firstBatchJob")
    public Job importUserJob(/*JobCompletionNotificationListener listener,*/ Step step) {
        return jobs.get("import-user")
                //.preventRestart()
                .incrementer(new RunIdIncrementer())
                //.listener(listener)
                .flow(step)
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcCursorItemReader  reader, ItemProcessor itemProcessor,  JdbcBatchItemWriter<PersonMerchant> writer) {

        return steps.get("get-step")
                .<Person, Person>chunk(1)
                .reader(reader)
                //.processor(itemProcessor)
                .writer(writer)
                //.allowStartIfComplete(true)
                .build();
    }

    //@Override
    private JobRepository getJobRepository(DataSource dataSource) throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDatabaseType("MYSQL");
        factory.setDataSource(dataSource);
        factory.setTransactionManager(new ResourcelessTransactionManager());
        //factory.setTablePrefix("SYSTEM.TEST_");
        return factory.getObject();
    }
    @Bean
    public JobLauncher getJobLauncher(DataSource dataSource) throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(getJobRepository(dataSource));
        //jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }

    @Scheduled(fixedDelay = 10000)
    @Bean
    @Primary
    public JobLauncher launchJob() throws Exception {
        return getJobLauncher(dataSource());
    }

    @AfterJob
    public void afterJob(JobExecution jobExecution) {
       System.out.println("parameters=" + jobExecution.getJobParameters()) ;
    }
}
