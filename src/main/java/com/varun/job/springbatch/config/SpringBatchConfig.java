package com.varun.job.springbatch.config;

import com.varun.job.springbatch.model.Person;
import com.varun.job.springbatch.model.PersonMerchant;
import com.varun.job.springbatch.processor.PersonItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

//@Configuration
//@EnableBatchProcessing
//@EnableScheduling
//@Component
public class SpringBatchConfig /*implements BatchConfigurer*/ {
    @Autowired
    private JobBuilderFactory jobs;
    @Autowired
    private StepBuilderFactory steps;
    @Autowired
    private SpringConfig springConfig;
    //@Autowired
    //private PersonItemProcessor personItemProcessor;
    /*@Autowired
    @Lazy
    private JobCompletionNotificationListener jobCompletionNotificationListener;*/

    @Bean
    public ItemReader<PersonMerchant> personMerchantItemReader(DataSource dataSource) {
        return new JdbcCursorItemReaderBuilder<PersonMerchant>()
                .name("person_merchant")
                .dataSource(dataSource)
                .sql("Select * From Person")
                .beanRowMapper(PersonMerchant.class)
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Person> reader() {
        return new FlatFileItemReaderBuilder<Person>()
                .name("PersonItemReader")
                .resource(new ClassPathResource("sample-data.csv"))
                .delimited()
                .names(new String[]{"id", "first_name", "last_name"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }
    @Bean
    @StepScope
    public JdbcBatchItemWriter<Person> writer() {
        return new JdbcBatchItemWriterBuilder<Person>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO person (id, first_name, last_name) VALUES (:id, :first_name, :last_name)")
                .dataSource(springConfig.dataSource())
                .build();
    }


    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate =  new JdbcTemplate();
        jdbcTemplate.setDataSource(springConfig.dataSource());
        return jdbcTemplate;
    }

    @Bean
    @StepScope
    public ItemProcessor<Person, Person> itemProcessor() {
        return new PersonItemProcessor();
    }

    /*@Bean
    public JobCompletionNotificationListener jobCompletionNotificationListener() {
        return new JobCompletionNotificationListener(jdbcTemplate());
    }*/



    @Bean(name = "firstBatchJob")
    public Job importUserJob(JobCompletionNotificationListener listener, Step step) {
        return jobs.get("import-user")
                //.preventRestart()
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step)
                .end()
                .build();
    }

    @Bean
    public Step step1(/*JdbcBatchItemWriter<Person> writer,*/ FlatFileItemReader<Person>  reader, ItemProcessor itemProcessor ) {

        return steps.get("get-step")
                .<Person, Person>chunk(2)
                .reader(reader)
                .processor(itemProcessor)
                //.writer(writer)
                //.allowStartIfComplete(true)
                .build();
    }

    //@Override
    private JobRepository getJobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDatabaseType("MYSQL");
        factory.setDataSource(springConfig.dataSource());
        factory.setTransactionManager(new ResourcelessTransactionManager());
        //factory.setTablePrefix("SYSTEM.TEST_");
        return factory.getObject();
    }

    /*@Override
    public PlatformTransactionManager getTransactionManager() throws Exception {
        return null;
    }
    @Bean
    public TransactionProxyFactoryBean baseProxy() {
        TransactionProxyFactoryBean transactionProxyFactoryBean = new TransactionProxyFactoryBean();
        Properties transactionAttributes = new Properties();
        transactionAttributes.setProperty("*", "PROPAGATION_REQUIRED");
        transactionProxyFactoryBean.setTransactionAttributes(transactionAttributes);
        transactionProxyFactoryBean.setTarget(jobRepository());
        transactionProxyFactoryBean.setTransactionManager(transactionManager());
        return transactionProxyFactoryBean;
    }*/

    //@Override
    //@Bean(name = "getjobLauncher")
    public JobLauncher getJobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(getJobRepository());
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }

    /*@Override
    public JobExplorer getJobExplorer() throws Exception {
        return null;
    }*/
    //@Scheduled(fixedDelay = 10000)
    //@Bean
    public void launchJob() throws Exception {
        //System.out.println("hello world");
        getJobLauncher();
        //JobLauncher jobLauncher = getJobLauncher();
        /*AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(SpringConfig.class);
        context.register(SpringBatchConfig.class);
        context.refresh();
        JobLauncher jobLauncher = (JobLauncher) context.getBean("getjobLauncher");
        Job job = (Job) context.getBean("firstBatchJob");
        System.out.println("Starting the batch job");
        try {
            JobExecution execution = jobLauncher.run(job, new JobParameters());
            System.out.println("Job Status : " + execution.getStatus());
            System.out.println("Job completed");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Job failed");
        }*/
    }
}
