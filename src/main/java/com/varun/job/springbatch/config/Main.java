package com.varun.job.springbatch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;

import javax.sql.DataSource;
import java.util.Date;

public class Main {
    public static void main(String[] args) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //context.register(DataSource.class);
        context.register(SpringConfig.class);
        context.register(SpringBatchConfigSQL.class);
        context.refresh();
        //DataSourceInitializer dataSource = new AnnotationConfigApplicationContext(SpringConfig.class).getBean(DataSourceInitializer.class);
        Job job = context.getBean(Job.class);
        JobLauncher jobLauncher = context.getBean(JobLauncher.class);
        System.out.println("jobName = " + job.getName());
        JobExecution jobExecution = jobLauncher.run(job, new JobParametersBuilder().addString("datetime", new Date().toString()).toJobParameters());
        System.out.println("running " + jobExecution.isRunning());
    }

}
