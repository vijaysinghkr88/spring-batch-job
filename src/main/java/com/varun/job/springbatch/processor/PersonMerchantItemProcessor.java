package com.varun.job.springbatch.processor;

import com.varun.job.springbatch.model.Person;
import com.varun.job.springbatch.model.PersonMerchant;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class PersonMerchantItemProcessor implements ItemProcessor<PersonMerchant, PersonMerchant> {
    @Override
    public PersonMerchant process(PersonMerchant personMerchant) throws Exception {
        return null;
    }
}
