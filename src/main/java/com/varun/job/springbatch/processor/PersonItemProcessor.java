package com.varun.job.springbatch.processor;

import com.varun.job.springbatch.model.Person;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

   // @Override
    public Person process(Person person) throws Exception {
        final int id = person.getId();
        final String firstName = person.getFirst_name();
        final String lastName = person.getLast_name();
        final Person transformedPerson = person;
        return transformedPerson;
    }
}
