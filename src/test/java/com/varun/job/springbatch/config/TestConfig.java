package com.varun.job.springbatch.config;

import org.springframework.batch.core.repository.support.SimpleJobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

//@Configuration
//@ComponentScan(value = "com.varun.job.springbatch.config")
public class TestConfig {
    //@Bean
    //@Primary
    public  DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        //dataSource.
        dataSource.setUrl("jdbc:mysql://localhost:3306/mybatchjob?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("testing");
        return dataSource;
    }

    @Bean
    public JobLauncherTestUtils jobLauncherTestUtils() {
        return new JobLauncherTestUtils();
    }
    //@Bean
    public JobRepositoryTestUtils jobRepositoryTestUtils() {
        JobRepositoryTestUtils jobRepositoryTestUtils = new JobRepositoryTestUtils();
        //jobRepositoryTestUtils.setJobRepository(new SimpleJobRepository());
        return new JobRepositoryTestUtils();
    }

}
