package com.varun.job.springbatch.config;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {SpringBatchConfig.class, SpringConfig.class})
//@EnableAutoConfiguration
//@ContextConfiguration(classes = { SpringBatchConfig.class })
/*@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)*/
class SpringBatchConfigTest {
    //@Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;
    //@Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;



    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        paramsBuilder.addString("file.input", "");
        paramsBuilder.addString("file.output", "");
        return paramsBuilder.toJobParameters();
    }

}