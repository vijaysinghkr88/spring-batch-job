package com.varun.job.springbatch.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes={SpringBatchConfigSQL.class, TestConfig.class})
public class SpringBatchConfigSQLTest {
    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    @Qualifier("firstBatchJob")
    private Job job;
   // @Autowired
   // JobRepositoryTestUtils jobRepositoryTestUtils;

    @Before
    public  void setUp() {
   //     jobRepositoryTestUtils.removeJobExecutions();
    }

    @Test
    public void testJob() throws Exception {
// when
        JobParametersBuilder builder = new JobParametersBuilder();
        builder.addDate("date", new Date());
        JobExecution jobExecution = jobLauncherTestUtils.getJobLauncher().run(job, builder.toJobParameters());
        //JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParameters());
        JobInstance actualJobInstance = jobExecution.getJobInstance();
        ExitStatus actualJobExitStatus = jobExecution.getExitStatus();
        //jobExecution.

        // then
        assertThat(actualJobInstance.getJobName(), is("import-user"));
        assertThat(actualJobExitStatus.getExitCode(), is("COMPLETED"));
    }

    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        //paramsBuilder.addString("p1", "paramter1");
        paramsBuilder.addString("datetime", new Date().toString());
        return paramsBuilder.toJobParameters();
    }

}